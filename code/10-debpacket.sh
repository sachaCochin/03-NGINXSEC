#/bin/bash

timestamp=$(date +%Y-%m-%d)
nginx_version='1.14.0'

mkdir -p /tmp/nginx_sec_$timestamp/debpacket
mkdir -p /tmp/nginx_sec_$timestamp/debpacket/DEBIAN

mkdir -p /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/etc/nginx
mkdir -p /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/sbin

cp -r /etc/nginx  /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/etc/nginx
cp    /sbin/nginx /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/sbin

echo "Package: nginxsec" > /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/control
echo "Version: $nginx_version" >> /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/control
echo "Maintainer: Marco Fiorani" >> /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/control
echo "Architecture: amd64" >> /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/control
echo "Description: nginx with modsecurity" >> /tmp/nginx_sec_$timestamp/debpacket/DEBIAN/control

dpkg-deb --build /tmp/nginx_sec_$timestamp/debpacket nginxsec.deb
mv nginxsec.deb ./packets/nginxsec.deb
