#/bin/bash

timestamp=$1
distro=$2
codename=$3
repofile=$4

# Configure the necessary repositories

echo "deb http://nginx.org/packages/mainline/$distro/ $codename nginx" > $repofile
echo "deb-src http://nginx.org/packages/mainline/$distro/ $codename nginx" > $repofile

wget http://nginx.org/keys/nginx_signing.key
apt-key add ./nginx_signing.key

# Update the server

apt update -y
apt upgrade -y

# Install required libraries

apt install -y apt-utils autoconf automake build-essential git libcurl4-openssl-dev libgeoip-dev liblmdb-dev libpcre++-dev libtool libxml2-dev libyajl-dev pkgconf wget zlib1g-dev libssl-dev build-essential devscripts debhelper

# Create temp directories

mkdir -p /tmp/nginx_sec_$timestamp/

# Remove nginx signing key
rm nginx_signing.key

exit
