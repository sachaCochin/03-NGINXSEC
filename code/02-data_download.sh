#/bin/bash

timestamp=$1
nginx_version=$2
base_path=$3

# Download the ModSecurity 3.0 Source Code

cd $base_path
git clone --depth 1 -b v3/master --single-branch https://github.com/SpiderLabs/ModSecurity

cd ModSecurity
git submodule init
git submodule update

# Download the NGINX Connector for ModSecurity 

cd $base_path
git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git

# Download NGINX source code

wget http://nginx.org/download/nginx-$nginx_version.tar.gz
tar zxvf nginx-$nginx_version.tar.gz

# Download Owasp Core rules

cd $base_path
wget https://github.com/SpiderLabs/owasp-modsecurity-crs/archive/v3.0/master.zip

exit
