#/bin/bash

timestamp=$1
nginx_version=$2
inst_path=$3
sbin_path=$4
conf_path=$5
pid_path=$6

nginx_conf="--with-debug --user=www-data --group=www-data --with-pcre-jit --with-http_ssl_module --with-http_realip_module --with-compat --prefix=/etc/nginx/ --prefix=$inst_path --sbin-path=$sbin_path --conf-path=$conf_path --pid-path=$pid_path --add-dynamic-module=../ModSecurity-nginx"

echo $inst_path
# 3 - Download and Compile the ModSecurity 3.0 Source Code

cd /tmp/nginx_sec_$timestamp/

cd ModSecurity
./build.sh
./configure
make
make install

# 4 -  NGINX Connector for ModSecurity 
# and Compile It as a Dynamic Module

cd /tmp/nginx_sec_$timestamp/
cd nginx-$nginx_version
./configure $nginx_conf

make modules
make 
make install
cp objs/ngx_http_modsecurity_module.so /etc/nginx/modules

exit
