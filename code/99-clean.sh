#!/bin/bash
timestamp=$(date +%Y-%m-%d)

#remove last 2 rows of /etc/apt/sources.list
sed -i '/nginx/d' /etc/apt/sources.list

#remove temp directories of compilationi
rm -fr /tmp/nginx_sec_$timestamp/

#remove installed files
rm -fr /etc/nginx
rm -f  /sbin/nginx
